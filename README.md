# Xbee Cellular + MQTT + Mosquitto

##### Materiales:
- Xbee Cellular 3G
- SIM NANO (Plan de datos)
- Antena con conector UFl
- Tarjeta de Desarrollo **XBIB-U-DEV**
- Cable USB tipo B


##### Para ejecutar el siguiente código se debe tomar en consideración:
- Clonar el proyecto
- [Descargar](https://www.jetbrains.com/pycharm/download/) el PyCharm
  - Descargar el plugins **Xbee Micro Python**
    - Settings > Plugins > **Xbee Micro Python**
![plugins_Pycharm](/uploads/40d38c7ea48bf3208b632be03d00aa7b/plugins_Pycharm.png) 
- Se debe abrir el proyecto en la carpeta `src/` para que pueda compilarse satisfactoriamente. 

##### Objetivos:
- El siguiente proyecto es un ejemplo de un dispositivo IoT, desarrollado con Xbee Cellular.
  - El dispositivo hace una suscripción al servidor de [mosquitto](https://test.mosquitto.org) del topico `xbee_topic/#`.
  - El dispositivo hace una petición a un servidor en la nube con el protocolo MQTT, con un tópico especifico, en este caso `xbee_topic/tempX`. 
  - Todas los dispositivos que se suscriban a ese tópico podrán recibir todo el mensaje enviado por el Xbee Cellular.
![suscrib](/uploads/c1131d30142d89ed1e4d694343c57d27/suscrib.png)
  - El Xbee Cellular 3G también le llegan todos los mensajes por que esta suscrito a todos los tópicos de `xbee_topic`.
![suscrib1](/uploads/7df2d8ec032fdefb075bae55598a7f52/suscrib1.png) 

##### Nota:
- Esto es un programa modificado de los ejemplos de Xbee MicroPython.